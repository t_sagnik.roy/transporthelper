import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:transporthelper/core/failure.dart';
import 'package:transporthelper/features/search_train/domain/entities/train_details_holder.dart';
import 'package:transporthelper/features/search_train/domain/repositories/train_search.dart';
import 'package:transporthelper/features/search_train/domain/use_cases/get_train_details_from_station_codes.dart';
import 'package:transporthelper/features/search_train/presentation/bloc/train_search_ui_bloc.dart';

import '../../domain/use_cases/get_train_details_from_station_code_test.dart';

final dummyTrainResponse = TrainDetailsHolder(
    trainNo: "12021",
    trainName: "testtrain",
    trainType: "T",
    trainOriginStation: "origin",
    trainOriginStationCode: "org",
    trainDestinationStation: "destination",
    trainDestinationStationCode: "dst",
    departTime: "10:10",
    arrivalTime: "10:10",
    distance: "1",
    classType: ["sl", "sl", "sl"],
    runDays: ["monday", "friday"]);

List<TrainDetailsHolder> trainList = [];

class MockTrainDetailsFromStationCode extends Mock
    implements TrainDetailsFromStationCode {
  @override
  Future<Either<Failure, List<TrainDetailsHolder>>> call(
      String startStationCode, String endStationCode) async {
    super.noSuchMethod(
        Invocation.method(#call, [startStationCode, endStationCode]),
        returnValue: Future.value(Right(trainList)));
    print(trainList);
    print("Inside false method");
    return Future.value(Right([dummyTrainResponse]));
  }
}

void main() {
  late MockTrainDetailsFromStationCode trainDetailsFromStationCode;
  late TrainSearchUiBloc trainSearchUiBloc;

  setUp(() {
    trainList = [];
    trainList.add(dummyTrainResponse);
    trainDetailsFromStationCode = MockTrainDetailsFromStationCode();
    trainSearchUiBloc = TrainSearchUiBloc(
        trainDetailsFromStationCode: trainDetailsFromStationCode);
  });

  test("initialState should be empty", () async {
    final result = await trainSearchUiBloc.state;
    expect(result, TrainSearchUiInitial());
  });

  group("getTrainDetailsFromStationCode", () {
    blocTest<TrainSearchUiBloc, TrainSearchUiState>(
        "should return valid data test using bloc_test",
        build: () {
          return trainSearchUiBloc;
        },
        act: (TrainSearchUiBloc trainSearchUiBloc) => trainSearchUiBloc.add(
            SearchTrainsUsingStationCode(
                fromStationCode: "Hwh", toStationCode: "csmt")),
        expect: () {
          return <TrainSearchUiState>[
            Loading(),
            Loaded(trainDeatails: trainList)
          ];
        });

    test("should return a list of trains when Station codes passed from UI",
        () async {
      when(trainDetailsFromStationCode("Hwh", "csmt"))
          .thenAnswer((_) async => Right(trainList));

      trainSearchUiBloc.add(SearchTrainsUsingStationCode(
          fromStationCode: "Hwh", toStationCode: "csmt"));

      expectLater(trainSearchUiBloc.stream,
          emitsInOrder([Loading(), Loaded(trainDeatails: trainList)]));
    }, timeout: Timeout(Duration(seconds: 60)));
  });
}
