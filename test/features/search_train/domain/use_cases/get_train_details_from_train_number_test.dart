// ignore_for_file: prefer_const_literals_to_create_immutables

import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:transporthelper/core/failure.dart';
import 'package:transporthelper/features/search_train/domain/entities/train_details_holder.dart';
import 'package:transporthelper/features/search_train/domain/repositories/train_search.dart';
import 'package:transporthelper/features/search_train/domain/use_cases/get_train_details_from_train_number.dart';

final dummyTrainResponse = TrainDetailsHolder(
    trainNo: "12021",
    trainName: "testtrain",
    trainType: "T",
    trainOriginStation: "origin",
    trainOriginStationCode: "org",
    trainDestinationStation: "destination",
    trainDestinationStationCode: "dst",
    departTime: "10:10",
    arrivalTime: "10:10",
    distance: "1",
    classType: ["sl", "sl", "sl"],
    runDays: ["monday", "friday"]);

class MockTrainSearch extends Mock implements TrainSearch {
  @override
  Future<Either<Failure, TrainDetailsHolder>> getTrainDetailsFromTrainNumber(
      String trainNumber) async {
    super.noSuchMethod(
        Invocation.method(#getTrainDetailsFromTrainNumber, [trainNumber]),
        returnValue: Future.value(Right(dummyTrainResponse)));

    return Future.value(Right(dummyTrainResponse));
  }
}

void main() {
  late TrainDetailsFromTrainNumber useCase;
  late MockTrainSearch mockTrainSearch;

  setUp(() {
    mockTrainSearch = MockTrainSearch();
    useCase = TrainDetailsFromTrainNumber(mockTrainSearch);
  });

  test("should return train details when train number is passed", () async {
    when(useCase("12021"))
        .thenAnswer((_) async => await Right(dummyTrainResponse));

    final result = await useCase("12021");

    expect(result, Right(dummyTrainResponse));
  });
}
