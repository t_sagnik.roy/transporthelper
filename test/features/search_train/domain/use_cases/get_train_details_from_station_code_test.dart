// ignore_for_file: prefer_const_literals_to_create_immutables

import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:transporthelper/core/failure.dart';
import 'package:transporthelper/features/search_train/domain/entities/train_details_holder.dart';

import 'package:transporthelper/features/search_train/domain/repositories/train_search.dart';
import 'package:transporthelper/features/search_train/domain/use_cases/get_train_details_from_station_codes.dart';

final dummyTrainResponse = TrainDetailsHolder(
    trainNo: "12021",
    trainName: "testtrain",
    trainType: "T",
    trainOriginStation: "origin",
    trainOriginStationCode: "org",
    trainDestinationStation: "destination",
    trainDestinationStationCode: "dst",
    departTime: "10:10",
    arrivalTime: "10:10",
    distance: "1",
    classType: ["sl", "sl", "sl"],
    runDays: ["monday", "friday"]);

List<TrainDetailsHolder> trainList = [];

class MockTrainSearch extends Mock implements TrainSearch {
  @override
  Future<Either<Failure, List<TrainDetailsHolder>>>
      getTrainsDetailsFromStationCode(
          String startStationCode, String endStationCode) async {
    trainList.add(dummyTrainResponse);
    super.noSuchMethod(
        Invocation.method(#getTrainDetailsFromStationCode,
            [startStationCode, endStationCode]),
        returnValue: Future.value(Right(trainList)));

    return Future.value(Right(trainList));
  }
}

void main() {
  late TrainDetailsFromStationCode useCase;
  late MockTrainSearch mockTrainSearch;

  setUp(() {
    mockTrainSearch = MockTrainSearch();
    useCase = TrainDetailsFromStationCode(mockTrainSearch);
  });

  test("should return list of train details when station codes are passed",
      () async {
    when(useCase("HWH", "CST"))
        .thenAnswer((_) async => await Right([dummyTrainResponse]));

    final result = await useCase("HWH", "CST");

    expect(result, Right(trainList));
  });
}
