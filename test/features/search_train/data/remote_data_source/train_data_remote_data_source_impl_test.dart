import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:http/testing.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:http/http.dart' as http;
import 'package:transporthelper/features/search_train/data/model/train_details_holder_model.dart';
import 'package:transporthelper/features/search_train/data/remote_data_source/train_data_remote_source.dart';

import '../../../fixtures/fixture_reader.dart';
import '../../domain/use_cases/get_train_details_from_station_code_test.dart';

List<TrainDetailsHolderModel> trainDetailsData = [];
final tTrainDetailsHolderModel = TrainDetailsHolderModel(
    trainNo: "18045",
    trainName: "East Coast Exp",
    trainType: "M",
    trainOriginStation: "Kolkata",
    trainOriginStationCode: "SHM",
    trainDestinationStation: "Hyderabad",
    trainDestinationStationCode: "HYB",
    departTime: "11:25:00",
    arrivalTime: "17:45:00",
    distance: "1587",
    classType: ["1A", "2A", "3A", "SL"],
    runDays: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]);

class MockHttpClient extends Mock implements http.Client {}

@GenerateMocks([http.Client])
void main() {
  late MockClient client;
  late TrainSearchRemoteDataSourceImpl trainSearchRemoteDataSourceImpl;

  setUp(() {
    trainDetailsData = [];
    trainDetailsData.add(tTrainDetailsHolderModel);
    client = MockClient((request) async {
      http.get(
        Uri.parse(
            "https://irctc1.p.rapidapi.com/api/v2/trainBetweenStations?fromStationCode=bju&toStationCode=bdts"),
        headers: {
          'X-RapidAPI-Key':
              'ce51cea62cmshaf27bfaa441a52ap1cb2f5jsn27df5e0be4c9',
          'X-RapidAPI-Host': 'irctc1.p.rapidapi.com'
        },
      );
      return http.Response(
          fixture("train_search_station_codes_response.json"), 200);
    });
    trainSearchRemoteDataSourceImpl = TrainSearchRemoteDataSourceImpl(client);
  });

  group("apiCallTest", () {
    test("should return a valid data when api is success", () async {
      when(
        client.get(
          Uri.parse(
              "https://irctc1.p.rapidapi.com/api/v2/trainBetweenStations?fromStationCode=bju&toStationCode=bdts"),
          headers: {
            'X-RapidAPI-Key':
                'ce51cea62cmshaf27bfaa441a52ap1cb2f5jsn27df5e0be4c9',
            'X-RapidAPI-Host': 'irctc1.p.rapidapi.com'
          },
        ),
      );

      final result = await trainSearchRemoteDataSourceImpl
          .getTrainsDetailsFromStationCode("Hwh", "csmt");

      expect(result, trainDetailsData);
    });
  });
}
