import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:transporthelper/core/failure.dart';
import 'package:transporthelper/features/search_train/data/model/train_details_holder_model.dart';
import 'package:transporthelper/features/search_train/data/remote_data_source/train_data_remote_source.dart';
import 'package:transporthelper/features/search_train/data/repositories/train_search_impl.dart';

List<TrainDetailsHolderModel> trainDetailsData = [];
final tTrainDetailsHolderModel = TrainDetailsHolderModel(
    trainNo: "18045",
    trainName: "East Coast Exp",
    trainType: "M",
    trainOriginStation: "Kolkata",
    trainOriginStationCode: "SHM",
    trainDestinationStation: "Hyderabad",
    trainDestinationStationCode: "HYB",
    departTime: "11:25:00",
    arrivalTime: "17:45:00",
    distance: "1587",
    classType: ["1A", "2A", "3A", "SL"],
    runDays: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]);

class MockTrainDataRemoteSource extends Mock
    implements TrainSearchRemoteDataSource {
  final tTrainDetailsHolderModel = TrainDetailsHolderModel(
      trainNo: "18045",
      trainName: "East Coast Exp",
      trainType: "M",
      trainOriginStation: "Kolkata",
      trainOriginStationCode: "SHM",
      trainDestinationStation: "Hyderabad",
      trainDestinationStationCode: "HYB",
      departTime: "11:25:00",
      arrivalTime: "17:45:00",
      distance: "1587",
      classType: ["1A", "2A", "3A", "SL"],
      runDays: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]);

  @override
  Future<List<TrainDetailsHolderModel>> getTrainsDetailsFromStationCode(
      String startStationCode, String endStationCode) async {
    super.noSuchMethod(
        Invocation.method(#getTrainsDetailsFromStationCode,
            [startStationCode, endStationCode]),
        returnValue: Future.value(trainDetailsData));

    return Future.value(trainDetailsData);
  }
}

void main() {
  late TrainSearchImpl repository;
  late MockTrainDataRemoteSource mockTrainDataRemoteSource;

  setUp(() {
    mockTrainDataRemoteSource = MockTrainDataRemoteSource();
    repository = TrainSearchImpl(mockTrainDataRemoteSource);
    trainDetailsData.add(tTrainDetailsHolderModel);
  });

  group("getTrainDetailsFromStationCodes", () {
    test("should return data when the station code api is called", () async {
      when(mockTrainDataRemoteSource.getTrainsDetailsFromStationCode(
              "HWH", "CSMT"))
          .thenAnswer((_) async => trainDetailsData);

      final result =
          await repository.getTrainsDetailsFromStationCode("HWH", "CSMT");

      expect(result, Right(trainDetailsData));
    });
  });
}
