import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:transporthelper/features/search_train/data/model/train_details_holder_model.dart';
import 'package:transporthelper/features/search_train/domain/entities/train_details_holder.dart';

import '../../../fixtures/fixture_reader.dart';

void main() {
  List<TrainDetailsHolderModel> trainDetailsData = [];
  final tTrainDetailsHolderModel = TrainDetailsHolderModel(
      trainNo: "18045",
      trainName: "East Coast Exp",
      trainType: "M",
      trainOriginStation: "Kolkata",
      trainOriginStationCode: "SHM",
      trainDestinationStation: "Hyderabad",
      trainDestinationStationCode: "HYB",
      departTime: "11:25:00",
      arrivalTime: "17:45:00",
      distance: "1587",
      classType: ["1A", "2A", "3A", "SL"],
      runDays: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]);

  trainDetailsData.add(tTrainDetailsHolderModel);

  test("TrainDetailsHolderModel should be a subclass of TrainDetailsHolder",
      () async {
    expect(trainDetailsData, isA<List<TrainDetailsHolder>>());
  });

  group("TrainDetailsFromJson", () {
    test(
        "should return a valid model from the Json data fetched (Train between 2 stations)",
        () async {
      final Map<String, dynamic> jsonMap =
          json.decode(fixture("train_search_station_codes_response.json"));

      final result = TrainDetailsHolderModel.trainDetailsFromJson(jsonMap);
      expect(result, trainDetailsData);
    });
  });
}
