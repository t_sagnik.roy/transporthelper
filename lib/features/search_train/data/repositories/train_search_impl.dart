import 'package:dartz/dartz.dart';
import 'package:transporthelper/core/failure.dart';
import 'package:transporthelper/features/search_train/domain/entities/train_details_holder.dart';
import 'package:transporthelper/features/search_train/domain/repositories/train_search.dart';

import '../remote_data_source/train_data_remote_source.dart';

class TrainSearchImpl extends TrainSearch {
  TrainSearchRemoteDataSource trainSearchRemoteDataSource;
  TrainSearchImpl(this.trainSearchRemoteDataSource);

  @override
  Future<Either<Failure, TrainDetailsHolder>> getTrainDetailsFromTrainNumber(
      String trainNo) {
    // TODO: implement getTrainDetailsFromTrainNumber
    throw UnimplementedError();
  }

  @override
  Future<Either<Failure, List<TrainDetailsHolder>>>
      getTrainsDetailsFromStationCode(
          String startStationCode, String endStationCode) async {
    final trainData = await trainSearchRemoteDataSource
        .getTrainsDetailsFromStationCode(startStationCode, endStationCode);
    return Right(trainData);
  }

  @override
  Future<Either<Failure, List<TrainDetailsHolder>>>
      getTrainsDetailsFromTrainName(String trainName) {
    // TODO: implement getTrainsDetailsFromTrainName
    throw UnimplementedError();
  }
}
