import 'package:transporthelper/features/search_train/domain/entities/train_details_holder.dart';

class TrainDetailsHolderModel extends TrainDetailsHolder {
  TrainDetailsHolderModel(
      {required super.trainNo,
      required super.trainName,
      required super.trainType,
      required super.trainOriginStation,
      required super.trainOriginStationCode,
      required super.trainDestinationStation,
      required super.trainDestinationStationCode,
      required super.departTime,
      required super.arrivalTime,
      required super.distance,
      required super.classType,
      required super.runDays});

  static List<TrainDetailsHolderModel> trainDetailsFromJson(
      Map<String, dynamic> json) {
    final List<dynamic> data = json["data"] as List;
    List<TrainDetailsHolderModel> trainsBetweenStation = [];
    int i;
    for (i = 0; i < data.length; i++) {
      trainsBetweenStation.add(TrainDetailsHolderModel(
          trainNo: data[i]["train_number"],
          trainName: data[i]["train_name"],
          trainType: data[i]["train_type"],
          trainOriginStation: data[i]["train_origin_station"],
          trainOriginStationCode: data[i]["train_origin_station_code"],
          trainDestinationStation: data[i]["train_destination_station"],
          trainDestinationStationCode: data[i]
              ["train_destination_station_code"],
          departTime: data[i]["depart_time"],
          arrivalTime: data[i]["arrival_time"],
          distance: data[i]["distance"],
          classType: data[i]["class_type"],
          runDays: data[i]["run_days"]));
    }
    return trainsBetweenStation;
  }
}
