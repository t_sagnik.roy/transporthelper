import 'dart:convert';

import 'package:transporthelper/features/search_train/data/model/train_details_holder_model.dart';
import 'package:transporthelper/features/search_train/data/repositories/train_search_impl.dart';
import 'package:http/http.dart' as http;

abstract class TrainSearchRemoteDataSource {
  Future<List<TrainDetailsHolderModel>> getTrainsDetailsFromStationCode(
      String startStationCode, String endStationCode);
}

class TrainSearchRemoteDataSourceImpl extends TrainSearchRemoteDataSource {
  http.Client client;
  TrainSearchRemoteDataSourceImpl(this.client);
  @override
  Future<List<TrainDetailsHolderModel>> getTrainsDetailsFromStationCode(
      String startStationCode, String endStationCode) async {
    final response = await client.get(
      Uri.parse(
          "https://irctc1.p.rapidapi.com/api/v2/trainBetweenStations?fromStationCode=bju&toStationCode=bdts"),
      headers: {
        'X-RapidAPI-Key': 'ce51cea62cmshaf27bfaa441a52ap1cb2f5jsn27df5e0be4c9',
        'X-RapidAPI-Host': 'irctc1.p.rapidapi.com'
      },
    );
    var data = json.decode(response.body) as Map<String, dynamic>;
    return TrainDetailsHolderModel.trainDetailsFromJson(data);
  }
}
