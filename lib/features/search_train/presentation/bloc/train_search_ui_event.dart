part of 'train_search_ui_bloc.dart';

abstract class TrainSearchUiEvent extends Equatable {
  const TrainSearchUiEvent();
}

class SearchTrainsUsingStationCode extends TrainSearchUiEvent {
  final String fromStationCode;
  final String toStationCode;

  SearchTrainsUsingStationCode(
      {required this.fromStationCode, required this.toStationCode});

  @override
  List<Object?> get props => [fromStationCode, toStationCode];
}
