// ignore_for_file: void_checks

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../domain/entities/train_details_holder.dart';
import '../../domain/use_cases/get_train_details_from_station_codes.dart';

part 'train_search_ui_event.dart';
part 'train_search_ui_state.dart';

class TrainSearchUiBloc extends Bloc<TrainSearchUiEvent, TrainSearchUiState> {
  TrainDetailsFromStationCode trainDetailsFromStationCode;

  TrainSearchUiBloc({required this.trainDetailsFromStationCode})
      : super(TrainSearchUiInitial()) {
    print("Hello moto");

    on<SearchTrainsUsingStationCode>((event, emit) async {
      emit(Loading());
      // final dummyTrainResponse = TrainDetailsHolder(
      //     trainNo: "12021",
      //     trainName: "testtrain",
      //     trainType: "T",
      //     trainOriginStation: "origin",
      //     trainOriginStationCode: "org",
      //     trainDestinationStation: "destination",
      //     trainDestinationStationCode: "dst",
      //     departTime: "10:10",
      //     arrivalTime: "10:10",
      //     distance: "1",
      //     classType: ["sl", "sl", "sl"],
      //     runDays: ["monday", "friday"]);

      final result = await trainDetailsFromStationCode.call(
          event.fromStationCode, event.toStationCode);

      result.fold((l) => emit(Error("Some error")),
          (r) => emit(Loaded(trainDeatails: r)));

      // List<TrainDetailsHolder> trainList = [];
      // trainList.add(dummyTrainResponse);
      // print("Intel inside");
      // emit(Loaded(trainDeatails: trainList));
      // print("Hello inside TrainSerchUIEvent");
      // emit(Loading());
      //
      // final result = await trainDetailsFromStationCode.call(
      //     event.fromStationCode, event.toStationCode);
      //
      // Stream<TrainSearchUiState> value = result.fold((l) async* {
      //   emit(Error("Some error occured"));
      // }, (data) async* {
      //   emit(Loaded(trainDeatails: data));
      // });
    });
  }
}
