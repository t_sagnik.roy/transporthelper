part of 'train_search_ui_bloc.dart';

abstract class TrainSearchUiState extends Equatable {
  const TrainSearchUiState();
}

class TrainSearchUiInitial extends TrainSearchUiState {
  @override
  List<Object> get props => [];
}

class Loading extends TrainSearchUiState {
  @override
  List<Object?> get props => [];
}

class Loaded extends TrainSearchUiState {
  final List<TrainDetailsHolder> trainDeatails;
  Loaded({required this.trainDeatails});

  @override
  List<Object?> get props => [trainDeatails];
}

class Error extends TrainSearchUiState {
  final String message;

  Error(this.message);

  @override
  List<Object?> get props => [message];
}
