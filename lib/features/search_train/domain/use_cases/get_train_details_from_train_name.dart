import 'package:transporthelper/core/failure.dart';
import 'package:dartz/dartz.dart';
import '../entities/train_details_holder.dart';
import '../repositories/train_search.dart';

class TrainDetailsFromTrainName {
  final TrainSearch trainSearchRepository;

  TrainDetailsFromTrainName(this.trainSearchRepository);

  Future<Either<Failure, List<TrainDetailsHolder>>> call(trainName) {
    return trainSearchRepository.getTrainsDetailsFromTrainName(trainName);
  }
}
