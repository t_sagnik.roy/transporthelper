import 'package:transporthelper/core/failure.dart';
import 'package:dartz/dartz.dart';
import '../entities/train_details_holder.dart';
import '../repositories/train_search.dart';

class TrainDetailsFromTrainNumber {
  final TrainSearch trainSearchRepository;

  TrainDetailsFromTrainNumber(this.trainSearchRepository);

  Future<Either<Failure, TrainDetailsHolder>> call(String trainNumber) {
    return trainSearchRepository.getTrainDetailsFromTrainNumber(trainNumber);
  }
}
