import 'package:transporthelper/core/failure.dart';
import 'package:dartz/dartz.dart';
import '../entities/train_details_holder.dart';
import '../repositories/train_search.dart';

class TrainDetailsFromStationCode {
  final TrainSearch trainSearchRepository;

  TrainDetailsFromStationCode(this.trainSearchRepository);

  Future<Either<Failure, List<TrainDetailsHolder>>> call(
      String startStationCode, String endStationCode) {
    return trainSearchRepository.getTrainsDetailsFromStationCode(
        startStationCode, endStationCode);
  }
}
