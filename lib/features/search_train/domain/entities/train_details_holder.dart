import 'package:equatable/equatable.dart';

class TrainDetailsHolder extends Equatable {
  final String trainNo;
  final String trainName;
  final String trainType;
  final String trainOriginStation;
  final String trainOriginStationCode;
  final String trainDestinationStation;
  final String trainDestinationStationCode;
  final String departTime;
  final String arrivalTime;
  final String distance;
  final List<dynamic> classType;
  final List<dynamic> runDays;

  TrainDetailsHolder({
    required this.trainNo,
    required this.trainName,
    required this.trainType,
    required this.trainOriginStation,
    required this.trainOriginStationCode,
    required this.trainDestinationStation,
    required this.trainDestinationStationCode,
    required this.departTime,
    required this.arrivalTime,
    required this.distance,
    required this.classType,
    required this.runDays,
  });

  @override
  List<Object?> get props => [
        trainNo,
        trainName,
        trainType,
        trainOriginStation,
        trainOriginStationCode,
        trainDestinationStation,
        trainDestinationStationCode,
        departTime,
        arrivalTime,
        distance,
        classType,
        runDays
      ];
}
