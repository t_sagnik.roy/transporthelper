import 'package:transporthelper/core/failure.dart';
import 'package:transporthelper/features/search_train/domain/entities/train_details_holder.dart';
import 'package:dartz/dartz.dart';

abstract class TrainSearch {
  Future<Either<Failure, TrainDetailsHolder>> getTrainDetailsFromTrainNumber(
      String trainNo);

  Future<Either<Failure, List<TrainDetailsHolder>>>
      getTrainsDetailsFromTrainName(String trainName);

  Future<Either<Failure, List<TrainDetailsHolder>>>
      getTrainsDetailsFromStationCode(
          String startStationCode, String endStationCode);
}
